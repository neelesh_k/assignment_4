# -*- coding: utf-8 -*-
"""
Helper file for logger and command line arguments
"""
from datetime import datetime
from json import load
from argparse import ArgumentParser
import logging
import logging.config

__author__ = "neelesh@gyandata.com"

LOGGER = logging.getLogger(name=__name__)


def initialize_logger():
    """
    Method to initialize and configure the logger

    :return: None
    """

    # Calling logging.json file
    log_file = "logging.json"
    with open(log_file, mode="r") as log_config_file:
        log_config_dict = load(fp=log_config_file)

    # Configuring logger
    logging.config.dictConfig(log_config_dict)

    LOGGER.info("LOGGER configuration Successful")


def initialize_command_args_transactions():
    """
    Method to initializes the command line arguments

    :return: It returns a tuple of

        * start_date - date from which the series starts

        * end_date - date from which the series ends

    :rtype: tuple
    """
    # Initializing parser
    parser = ArgumentParser()

    # Adding arguments
    parser.add_argument("-sd",
                        "--start_date",
                        type=lambda s: datetime.strptime(s, '%Y-%m-%d').date(),
                        help="The date from which the series starts in %%Y-%%m-%%d format. Example: 2021-05-06",
                        required=True)

    parser.add_argument("-ed",
                        "--end_date",
                        type=lambda s: datetime.strptime(s, '%Y-%m-%d').date(),
                        help="The date from which the series ends in %%Y-%%m-%%d format. Example: 2021-06-09",
                        required=True)

    parser.add_argument("-exchange",
                        "--exchange_rate",
                        type=float,
                        help="Exchange rate. Example: 74.44",
                        required=True)

    args = parser.parse_args()
    start_date = args.start_date
    end_date = args.end_date
    exchange_rate = args.exchange_rate

    return start_date, end_date, exchange_rate


def initialize_command_args_branch_error():
    """
    Method to initializes the command line arguments

    :return: min_discount
    :rtype: float
    """
    # Initializing parser
    parser = ArgumentParser()

    # Adding arguments
    parser.add_argument("-min_dis",
                        "--min_discount",
                        type=float,
                        help="Minimum discount given",
                        required=True)

    args = parser.parse_args()
    min_discount = args.min_discount

    return min_discount


def initialize_command_args_branch_revenue():
    """
    Method to initializes the command line arguments

    :return: new_name
    :rtype: str
    """
    # Initializing parser
    parser = ArgumentParser()

    # Adding arguments
    parser.add_argument("-new_name",
                        "--new_branch_name",
                        type=str,
                        help="New branch name",
                        required=True)

    args = parser.parse_args()
    new_branch_name = args.new_branch_name

    return new_branch_name


def initialize_command_args_customer_age():
    """
    Method to initializes the command line arguments

    :return: address
    :rtype: str
    """
    # Initializing parser
    parser = ArgumentParser()

    # Adding arguments
    parser.add_argument("-loc",
                        "--address",
                        type=str,
                        help="Target location. Example : New York, USA",
                        required=True)

    args = parser.parse_args()
    address = args.address

    return address


def initialize_command_args_staff_rename():
    """
    Method to initializes the command line arguments

    :return: It returns a tuple of
                * old_first_name
                * old_last_name
                * new_first_name
                * new_last_name

    :rtype: tuple
    """
    # Initializing parser
    parser = ArgumentParser()

    # Adding arguments
    parser.add_argument("-old_first",
                        "--old_first_name",
                        type=str,
                        help="Old first name of the staff. Example : Holly",
                        required=True)

    parser.add_argument("-old_last",
                        "--old_last_name",
                        type=str,
                        help="Old last name of the staff. Example : Flax",
                        required=True)

    parser.add_argument("-new_first",
                        "--new_first_name",
                        type=str,
                        help="New first name of the staff. Example : Hollie",
                        required=True)

    parser.add_argument("-new_last",
                        "--new_last_name",
                        type=str,
                        help="New last name of the staff. Example : Scott",
                        required=True)

    args = parser.parse_args()
    old_first_name = args.old_first_name
    old_last_name = args.old_last_name
    new_first_name = args.new_first_name
    new_last_name = args.new_last_name

    return old_first_name, old_last_name, new_first_name, new_last_name
