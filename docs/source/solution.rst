Assignment IV Solution
-------------------------

ABC SuperMarket is venturing into online business (purchase and supply of products). They
need your services to help in the logistics and delivery of their products with their purchase
history. For example, you will be asked to infer the demand of product X in a particular
location Y, which would help them stock the products and appoint delivery personnel
accordingly. You are asked to design an interface to extract as much information from their
POS (Point of Sales) transaction history. The communication between the POS system and our development machine is through
APIs exposed as web service.

Your task will be the following:
    ● Give an example response with the contents for each of the tables in the schema.
    The response should be in the form of json. (Please note that only the primary keys
    and foreign keys are mentioned in the schema. You can assume the other attributes)

    ● Design schema classes for validating the responses obtained using the marshmallow
    library.

    ● Given an input dictionary in the form of a JSON construct an instance of a class
    without explicitly calling the class constructor.

    ● Give user readable validation errors in case of any.

.. toctree::
   :maxdepth: 4

   solution.class_design
   solution.class_schema
   solution.object_creation
   solution.queries
   solution.deserialize_objects
