Constructing Classes in Python
==============================

.. automodule:: solution.class_design
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

.. image:: design.png
  :width: 1200
  :alt: Class design


Branch, Staff, Manager and Customer Class
-------------------------------------------

.. toctree::
   :maxdepth: 4

   solution.class_design.branch
   solution.class_design.person
   solution.class_design.staff
   solution.class_design.manager
   solution.class_design.customer

Product, Transaction, and Purchase Class
-----------------------------------------

.. toctree::
   :maxdepth: 4

   solution.class_design.product
   solution.class_design.transaction
   solution.class_design.purchase


