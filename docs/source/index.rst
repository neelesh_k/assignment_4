.. Assignment 4 : Serialization and Deserialization documentation master file, created by
   sphinx-quickstart on Thu Jul 15 09:44:35 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Assignment 4 : Serialization and Deserialization
=================================================

.. toctree::
   :maxdepth: 2


   solution

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
