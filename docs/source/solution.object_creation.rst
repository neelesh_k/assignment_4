Creating objects of the classes
================================

.. automodule:: solution.object_creation
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:


.. toctree::
   :maxdepth: 4

   solution.object_creation.other_objects
   solution.object_creation.purchase_objects
