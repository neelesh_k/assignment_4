Performing Queries
========================

.. automodule:: solution.queries
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

.. toctree::
   :maxdepth: 4

   solution.queries.branch_errors
   solution.queries.customer_age
   solution.queries.manager_rename
   solution.queries.staff_promotion
   solution.queries.transaction_inr
