Designing Marshmallow Schemas
===============================

.. automodule:: solution.class_schema
   :members:
   :undoc-members:

Branch, Staff, Manager and Customer Schema
-------------------------------------------

.. toctree::
   :maxdepth: 4

   solution.class_schema.branch_schema
   solution.class_schema.staff_schema
   solution.class_schema.manager_schema
   solution.class_schema.customer_schema

Product, Transaction, and Purchase Schema
-----------------------------------------

.. toctree::
   :maxdepth: 4

   solution.class_schema.custom_fields
   solution.class_schema.product_schema
   solution.class_schema.transaction_schema
   solution.class_schema.purchase_schema

