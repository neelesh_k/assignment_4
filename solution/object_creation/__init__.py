# -*- coding: utf-8 -*-
"""
A class describes the contents of the objects that belong to it: it describes an aggregate of data fields (called
instance variables), and defines the operations (called methods). object: an object is an element (or instance) of a
class; objects have the behaviors of their class.
"""

import logging

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)
