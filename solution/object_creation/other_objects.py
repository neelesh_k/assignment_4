# -*- coding: utf-8 -*-
"""
Branch, Customer, Staff, Manager and Product Objects
------------------------------------------------------

"""
import logging
from datetime import date
from solution.class_design.branch import Branch
from solution.class_design.customer import Customer
from solution.class_design.manager import Manager
from solution.class_design.staff import Staff
from solution.class_design.product import Product

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def create_branches():
    """
    Constructs object of the Branch class

    :return: tuple of Branch objects
    :rtype: tuple
    """
    scranton = Branch(branch_id=1,
                      name="Dunder Mifflin, Scranton",
                      address="Scranton, Pennsylvania, USA",
                      pincode="600092",
                      year_opened=1981)

    utica = Branch(branch_id=2,
                   name="Dunder Mifflin, Utica",
                   address="Utica, Pennsylvania, USA",
                   pincode="600093",
                   year_opened=2018)

    new_york = Branch(branch_id=3,
                      name="Dunder Mifflin, New York",
                      address="New York, USA",
                      pincode="600102",
                      year_opened=1985)

    stamford = Branch(branch_id=4,
                      name="Dunder Mifflin, Stamford",
                      address="Stamford, Pennsylvania, USA",
                      pincode="600095",
                      year_opened=1969)

    return scranton, utica, new_york, stamford


def create_customers():
    """
    Constructs object of the Customer class

    :return: tuple of Customer objects
    :rtype: tuple
    """
    jim = Customer(customer_id=1,
                   first_name="Jim",
                   last_name="Halpert",
                   dob=date(1980, 6, 9),
                   email="jim@gmail.com",
                   phone="9999999999",
                   address="New York, USA")

    pam = Customer(customer_id=2,
                   first_name="Pam",
                   last_name="Beesley",
                   dob=date(1985, 9, 6),
                   email="pam@gmail.com",
                   phone="9999999999",
                   address="New York, USA")

    ryan = Customer(customer_id=3,
                    first_name="Ryan",
                    last_name="Howard",
                    dob=date(1996, 12, 19),
                    email="ryan@gmail.com",
                    phone="9999999999",
                    address="Philadelphia, USA")

    kelly = Customer(customer_id=4,
                     first_name="Kelly",
                     last_name="Kapoor",
                     dob=date(1988, 3, 6),
                     email="kelly@gmail.com",
                     phone="9999999999",
                     address="New York, USA")

    holly = Customer(customer_id=5,
                     first_name="Holly",
                     last_name="Flax",
                     dob=date(1976, 2, 24),
                     email="holly@gmail.com",
                     phone="9999999999",
                     address="Philadelphia, USA")

    jan = Customer(customer_id=6,
                   first_name="Jan",
                   last_name="Levinson",
                   dob=date(1979, 12, 15),
                   email="jan@gmail.com",
                   phone="9999999999",
                   address="Philadelphia, USA")

    toby = Customer(customer_id=7,
                    first_name="Toby",
                    last_name="Flenderson",
                    dob=date(1958, 3, 31),
                    email="toby@gmail.com",
                    phone="9999999999",
                    address="New York, USA")

    return jim, pam, ryan, kelly, holly, jan, toby


def create_managers():
    """
    Constructs object of the Staff class

    :return: tuple of Manager objects
    :rtype: tuple
    """
    micheal = Manager(manager_id=1,
                      first_name="Micheal",
                      last_name="Scott",
                      level=5,
                      salary=350000,
                      dob=date(1960, 12, 26),
                      email="micheal@dundermifflin.com",
                      phone="9999999999",
                      address="Scranton, USA")

    david = Manager(manager_id=2,
                    first_name="David",
                    last_name="Wallace",
                    level=5,
                    salary=325000,
                    dob=date(1980, 5, 9),
                    email="david@dundermifflin.com",
                    phone="9999999999",
                    address="New York, USA")

    karen = Manager(manager_id=3,
                    first_name="Karen",
                    last_name="Phillipe",
                    level=5,
                    salary=315000,
                    dob=date(1983, 12, 11),
                    email="karen@dundermifflin.com",
                    phone="9999999999",
                    address="Utica, USA")

    return micheal, david, karen


def create_staffs():
    """
    Constructs object of the Staff class

    :return: tuple of Staff objects
    :rtype: tuple
    """

    micheal, david, karen = create_managers()

    stanley = Staff(staff_id=1,
                    first_name="Stanley",
                    last_name="Hudson",
                    level=4,
                    salary=150000,
                    manager=micheal,
                    dob=date(1960, 2, 16),
                    email="stanley@dundermifflin.com",
                    phone="9999999999",
                    address="Scranton, USA")

    dwight = Staff(staff_id=2,
                   first_name="Dwight",
                   last_name="Schrute",
                   level=3,
                   salary=120000,
                   manager=micheal,
                   dob=date(1983, 11, 21),
                   email="dwight@dundermifflin.com",
                   phone="9999999999",
                   address="Scranton, USA")

    andrew = Staff(staff_id=3,
                   first_name="Andrew",
                   last_name="Bernard",
                   level=3,
                   salary=120000,
                   manager=karen,
                   dob=date(1984, 3, 5),
                   email="andrew@dundermifflin.com",
                   phone="9999999999",
                   address="Stamford, USA")

    creed = Staff(staff_id=4,
                  first_name="Creed",
                  last_name="Bratton",
                  level=4,
                  salary=170000,
                  manager=karen,
                  dob=date(1957, 2, 15),
                  email="creed@dundermifflin.com",
                  phone="9999999999",
                  address="California, USA")

    oscar = Staff(staff_id=5,
                  first_name="Oscar",
                  last_name="Martinez",
                  manager=david,
                  level=3,
                  salary=100000,
                  dob=date(1988, 10, 23),
                  email="oscar@dundermifflin.com",
                  phone="9999999999",
                  address="Ohio, USA")

    kevin = Staff(staff_id=6,
                  first_name="Kevin",
                  last_name="Malone",
                  manager=david,
                  level=4,
                  salary=150000,
                  dob=date(1981, 9, 1),
                  email="kevin@dundermifflin.com",
                  phone="9999999999",
                  address="Wisconsin, USA")

    angela = Staff(staff_id=7,
                   first_name="Angela",
                   last_name="Martin",
                   manager=karen,
                   level=3,
                   salary=130000,
                   dob=date(1989, 8, 10),
                   email="angela@dundermifflin.com",
                   phone="9999999999",
                   address="Michigan, USA")

    return stanley, dwight, andrew, creed, oscar, kevin, angela


def create_products():
    """
    Constructs object of the Product class and serializes as json files.

    :return: tuple of Product objects
    :rtype: tuple
    """
    macbook_air = Product(product_id=1,
                          name="Macbook Air M1",
                          category="Laptops and Tablets",
                          manufacturer="Apple Inc.",
                          price_usd=999,
                          year_manufactured=2020)

    maggi_noodles = Product(product_id=2,
                            name="Maggi Noodles",
                            category="Instant-food",
                            manufacturer="Nestle",
                            price_usd=2.50,
                            year_manufactured=2021)

    parker_pen = Product(product_id=3,
                         name="Parker Pen Gel",
                         category="Stationary",
                         manufacturer="Parker",
                         price_usd=4.99,
                         year_manufactured=2019)

    galaxy_note20 = Product(product_id=4,
                            name="Galaxy Note 20",
                            category="Mobile Phones",
                            manufacturer="Samsung",
                            price_usd=849,
                            year_manufactured=2020)

    nike_shirt = Product(product_id=5,
                         name="Nike sport jersey",
                         category="Apparel",
                         manufacturer="Nike",
                         price_usd=14,
                         year_manufactured=2013)

    toblerone_chocolate = Product(product_id=6,
                                  name="Toblerone chocolate bar",
                                  category="Confectionary",
                                  manufacturer="Toblerone",
                                  price_usd=9,
                                  year_manufactured=2021)

    ipad_pro = Product(product_id=7,
                       name="iPad Pro M1",
                       category="Laptops and Tablets",
                       manufacturer="Apple Inc.",
                       price_usd=849,
                       year_manufactured=2020)

    return macbook_air, maggi_noodles, parker_pen, galaxy_note20, nike_shirt, toblerone_chocolate, ipad_pro
