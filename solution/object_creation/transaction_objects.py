# -*- coding: utf-8 -*-
"""
Transaction and Purchase objects
--------------------------------

"""
import json
import logging
from datetime import date
from solution.class_design.transaction import Transaction
from solution.class_design.purchase import Purchase
from solution.class_schema.transaction_schema import TransactionSchema
from solution.object_creation.other_objects import create_branches, create_customers
from solution.object_creation.other_objects import create_staffs, create_products

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def create_objects():
    """
    Constructs object of the respective classes and serializes as json files.

    :return: None
    :rtype: None
    """
    # branch objects
    scranton, utica, new_york, stamford = create_branches()

    # customer objects
    jim, pam, ryan, kelly, holly, jan, toby = create_customers()

    # staff objects
    stanley, dwight, andrew, creed, oscar, kevin, angela = create_staffs()

    # product objects
    macbook_air, maggi_noodles, parker_pen, galaxy_note20, nike_shirt, toblerone, ipad_pro = create_products()

    # purchase objects
    purchase_1 = Purchase(purchase_id=1,
                          product=macbook_air,
                          num_units=1)

    purchase_2 = Purchase(purchase_id=2,
                          product=maggi_noodles,
                          num_units=5)

    purchase_3 = Purchase(purchase_id=3,
                          product=toblerone,
                          num_units=460)

    purchase_4 = Purchase(purchase_id=4,
                          product=galaxy_note20,
                          num_units=1)

    purchase_5 = Purchase(purchase_id=5,
                          product=nike_shirt,
                          num_units=10)

    purchase_6 = Purchase(purchase_id=6,
                          product=parker_pen,
                          num_units=30)

    purchase_7 = Purchase(purchase_id=7,
                          product=nike_shirt,
                          num_units=18)

    purchase_8 = Purchase(purchase_id=8,
                          product=parker_pen,
                          num_units=10)

    purchase_9 = Purchase(purchase_id=9,
                          product=ipad_pro,
                          num_units=2)

    purchase_10 = Purchase(purchase_id=10,
                           product=ipad_pro,
                           num_units=1)

    purchase_11 = Purchase(purchase_id=11,
                           product=macbook_air,
                           num_units=1)

    purchase_12 = Purchase(purchase_id=12,
                           product=maggi_noodles,
                           num_units=3)

    purchase_13 = Purchase(purchase_id=13,
                           product=toblerone,
                           num_units=460)

    purchase_14 = Purchase(purchase_id=14,
                           product=galaxy_note20,
                           num_units=1)

    purchase_15 = Purchase(purchase_id=15,
                           product=nike_shirt,
                           num_units=10)

    purchase_16 = Purchase(purchase_id=16,
                           product=parker_pen,
                           num_units=32)

    purchase_17 = Purchase(purchase_id=17,
                           product=nike_shirt,
                           num_units=19)

    purchase_18 = Purchase(purchase_id=18,
                           product=parker_pen,
                           num_units=11)

    purchase_19 = Purchase(purchase_id=19,
                           product=ipad_pro,
                           num_units=2)

    purchase_20 = Purchase(purchase_id=20,
                           product=ipad_pro,
                           num_units=1)

    # transaction objects
    transaction_1 = Transaction(trans_id=1,
                                customer=jim,
                                staff=stanley,
                                branch=scranton,
                                purchases_list=[purchase_16, purchase_2, purchase_3],
                                trans_amount=869.10,
                                trans_date=date(2021, 7, 11),
                                discount_percent=10)

    transaction_2 = Transaction(trans_id=2,
                                customer=toby,
                                staff=creed,
                                branch=new_york,
                                purchases_list=[purchase_4, purchase_17, purchase_5],
                                trans_amount=30,
                                trans_date=date(2021, 7, 15),
                                discount_percent=6)

    transaction_3 = Transaction(trans_id=3,
                                customer=jim,
                                staff=dwight,
                                branch=utica,
                                purchases_list=[purchase_7, purchase_2, purchase_18],
                                trans_amount=3999,
                                trans_date=date(2021, 7, 10),
                                discount_percent=0)

    transaction_4 = Transaction(trans_id=4,
                                customer=ryan,
                                staff=angela,
                                branch=stamford,
                                purchases_list=[purchase_1, purchase_19, purchase_10],
                                trans_amount=835.325,
                                trans_date=date(2021, 7, 12),
                                discount_percent=7.5)

    transaction_5 = Transaction(trans_id=5,
                                customer=pam,
                                staff=stanley,
                                branch=scranton,
                                purchases_list=[purchase_11, purchase_12, purchase_3],
                                trans_amount=108,
                                trans_date=date(2021, 7, 17),
                                discount_percent=20)

    transaction_6 = Transaction(trans_id=6,
                                customer=toby,
                                staff=dwight,
                                branch=new_york,
                                purchases_list=[purchase_14, purchase_2, purchase_13],
                                trans_amount=220,
                                trans_date=date(2021, 7, 9),
                                discount_percent=9)

    transaction_7 = Transaction(trans_id=7,
                                customer=jim,
                                staff=andrew,
                                branch=stamford,
                                purchases_list=[purchase_15, purchase_20, purchase_3],
                                trans_amount=210,
                                trans_date=date(2021, 7, 14),
                                discount_percent=2.5)

    transaction_8 = Transaction(trans_id=8,
                                customer=ryan,
                                staff=dwight,
                                branch=scranton,
                                purchases_list=[purchase_12, purchase_2, purchase_5],
                                trans_amount=66.91,
                                trans_date=date(2021, 7, 6),
                                discount_percent=10)

    transaction_9 = Transaction(trans_id=9,
                                customer=holly,
                                staff=oscar,
                                branch=new_york,
                                purchases_list=[purchase_6, purchase_12, purchase_5],
                                trans_amount=1463.3,
                                trans_date=date(2021, 7, 10),
                                discount_percent=12.5)

    transaction_10 = Transaction(trans_id=10,
                                 customer=jan,
                                 staff=kevin,
                                 branch=scranton,
                                 purchases_list=[purchase_10, purchase_12, purchase_13],
                                 trans_amount=879,
                                 trans_date=date(2021, 7, 25),
                                 discount_percent=17)

    transaction_11 = Transaction(trans_id=11,
                                 customer=holly,
                                 staff=stanley,
                                 branch=scranton,
                                 purchases_list=[purchase_2, purchase_2, purchase_4],
                                 trans_amount=889.10,
                                 trans_date=date(2021, 7, 23),
                                 discount_percent=11)

    transaction_12 = Transaction(trans_id=12,
                                 customer=toby,
                                 staff=dwight,
                                 branch=new_york,
                                 purchases_list=[purchase_1, purchase_2, purchase_3],
                                 trans_amount=20,
                                 trans_date=date(2021, 7, 11),
                                 discount_percent=9)

    transaction_13 = Transaction(trans_id=13,
                                 customer=jan,
                                 staff=andrew,
                                 branch=utica,
                                 purchases_list=[purchase_11, purchase_20, purchase_13],
                                 trans_amount=3879,
                                 trans_date=date(2021, 7, 21),
                                 discount_percent=5.5)

    transaction_14 = Transaction(trans_id=14,
                                 customer=kelly,
                                 staff=creed,
                                 branch=stamford,
                                 purchases_list=[purchase_5, purchase_2, purchase_6],
                                 trans_amount=825.325,
                                 trans_date=date(2021, 7, 22),
                                 discount_percent=7.5)

    transaction_15 = Transaction(trans_id=15,
                                 customer=toby,
                                 staff=oscar,
                                 branch=scranton,
                                 purchases_list=[purchase_9, purchase_2, purchase_8],
                                 trans_amount=88,
                                 trans_date=date(2021, 7, 20),
                                 discount_percent=17)

    transaction_16 = Transaction(trans_id=16,
                                 customer=kelly,
                                 staff=kevin,
                                 branch=new_york,
                                 purchases_list=[purchase_6, purchase_9, purchase_10],
                                 trans_amount=220,
                                 trans_date=date(2021, 7, 9),
                                 discount_percent=18)

    transaction_17 = Transaction(trans_id=17,
                                 customer=jim,
                                 staff=angela,
                                 branch=stamford,
                                 purchases_list=[purchase_6, purchase_4, purchase_20],
                                 trans_amount=230,
                                 trans_date=date(2021, 7, 14),
                                 discount_percent=2.5)

    transaction_18 = Transaction(trans_id=18,
                                 customer=ryan,
                                 staff=stanley,
                                 branch=scranton,
                                 purchases_list=[purchase_9, purchase_12, purchase_4],
                                 trans_amount=86.91,
                                 trans_date=date(2021, 7, 11),
                                 discount_percent=8)

    transaction_19 = Transaction(trans_id=19,
                                 customer=toby,
                                 staff=dwight,
                                 branch=new_york,
                                 purchases_list=[purchase_11, purchase_12, purchase_13],
                                 trans_amount=1433.3,
                                 trans_date=date(2021, 7, 15),
                                 discount_percent=4)

    transaction_20 = Transaction(trans_id=20,
                                 customer=jan,
                                 staff=andrew,
                                 branch=scranton,
                                 purchases_list=[purchase_6, purchase_9, purchase_4],
                                 trans_amount=869,
                                 trans_date=date(2021, 7, 19),
                                 discount_percent=6)

    transactions = [transaction_1, transaction_2, transaction_3, transaction_4, transaction_5, transaction_6,
                    transaction_7, transaction_8, transaction_9, transaction_10, transaction_11, transaction_12,
                    transaction_13, transaction_14, transaction_15, transaction_16, transaction_17, transaction_18,
                    transaction_19, transaction_20]

    schema = TransactionSchema()
    json_data = schema.dump(transactions, many=True)

    # dumping to a json file
    with open(r"../../transactions.json", "w") as json_file:
        json.dump(json_data, json_file)
    LOGGER.debug("transactions.json file created.")


if __name__ == '__main__':
    create_objects()
