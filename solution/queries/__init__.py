# -*- coding: utf-8 -*-
"""

1. Filter the transactions by discount offered and calculate the discrepancies in each branch
2. Display the customer ages in a given location
3. Rename a Manager in the json file
4. Find the staff that generated the most revenue from a particular product and promote them
5. Filter the transactions between two dates and get the total amount in INR

"""

import logging

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)
