# -*- coding: utf-8 -*-
"""
Staff promotion
-----------------

Find the staff that generated the most revenue from a particular product and promote him/her

"""
import logging
from solution.deserialize_objects import deserialize_transactions

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def promote_best_employee(transaction_json_list, ):
    """
    Promotes the staff that made the most number of sales of the product

    :param transaction_json_list: json file
    :type transaction_json_list: list

    :return: staff_revenue
    :rtype: dict

    :raises ValueError: There should be at least one transaction of the specified product.

    .. note::
        1. Filters and creates a new json file based on the product
        2. Serializes the filtered json file as a list of Transaction objects
        3. Iterates over the list and finds the revenue made by each staff
        4. Finds the staff who made the maximum revenue and promotes him/her
        5. Returns a dictionary of customer name and age
    """

    # deserializing the new json file into list of objects
    transaction_object_list = deserialize_transactions(transaction_json_list)

    try:
        # initialization
        staff_revenue = dict()

        # iterating the given list of json values
        for transaction in transaction_object_list:
            # extracting staff_name and sales_amount
            staff_name = transaction.staff.first_name
            sales_amount = transaction.trans_amount

            # adding up the trans_amount corresponding to each staff_name
            if staff_name in staff_revenue:
                staff_revenue[staff_name] += sales_amount
            else:
                staff_revenue[staff_name] = sales_amount

        # staff with highest sum of trans_amount
        max_revenue_staff = max(staff_revenue, key=staff_revenue.get)

        # displaying max_revenue_staff and max_revenue_amt
        LOGGER.info("Best employee : %s", max_revenue_staff)

        # iterating the list of Transaction objects
        for transaction in transaction_object_list:
            # getting the first instance of max_revenue_staff and exiting the loop
            if transaction.staff.first_name == max_revenue_staff:
                # displaying existing level and salary
                LOGGER.info("Current level of %s : %d", max_revenue_staff, transaction.staff.level)
                LOGGER.info("Current salary of %s : %d", max_revenue_staff, transaction.staff.salary)
                # performing Staff method operation
                transaction.staff.promote_staff()
                # displaying new level and salary
                LOGGER.info("New level of %s : %d", max_revenue_staff, transaction.staff.level)
                LOGGER.info("New salary of %s : %d", max_revenue_staff, transaction.staff.salary)
                break

        return staff_revenue

    except ValueError:
        LOGGER.error("No Transactions made")
        raise
