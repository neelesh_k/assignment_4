# -*- coding: utf-8 -*-
"""
Customer Age Statistics
----------------------------------

**Mean**

Mean is an arithmetic average of the data set and it can be calculated by dividing a sum of all the data points with
the number of data points in the data set. It is a point in a data set that is the average of all the data points we
have in a set.

**Median**

In statistics and probability theory, the median is the value separating the higher half from the lower half of a
data sample, a population, or a probability distribution. For a data set, it may be thought of as "the middle" value.

**Standard Deviation**

In statistics, the standard deviation is a measure of the amount of variation or dispersion of a set of values. A low
standard deviation indicates that the values tend to be close to the mean of the set, while a high standard deviation
indicates that the values are spread out over a wider range.
"""

import logging
import math
from solution.deserialize_objects import deserialize_transactions

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def compute_mean(given_list):
    """
    Finds the mean value for the given list

    :param given_list: list of values
    :type  given_list: list

    :return: mean
    :rtype: float
    """
    total = sum(given_list)
    count = len(given_list)

    return total / count


def compute_median(given_list):
    """
    Finds the mean value for the given list

    :param given_list: list of values
    :type  given_list: list

    :return: median
    :rtype: float
    """
    given_list.sort()
    count = len(given_list)

    if count % 2 == 0:
        median1 = given_list[count // 2]
        median2 = given_list[count // 2 - 1]
        median = (median1 + median2) / 2
    else:
        median = given_list[count // 2]

    return median


def compute_standard_dev(given_list):
    """
    Finds the standard deviation value for the given list

    :param given_list: list of values
    :type  given_list: list

    :return: standard_dev
    :rtype: float
    """
    mean = compute_mean(given_list)
    deviations = [(num - mean) ** 2 for num in given_list]
    variance = sum(deviations) / len(given_list)
    standard_dev = math.sqrt(variance)

    return standard_dev


def filter_by_address(transaction, address):
    """
    filters the list of objects based on the customer address

    :param transaction: transaction object
    :type transaction: transaction

    :param address: customer address
    :type address: str

    :return: transaction object
    :rtype: transaction
    """
    return transaction["customer"]["address"] == address


def get_customer_stats(transaction_json_list, address):
    """
    Returns a dictionary of customer name and age in a particular address

    :param transaction_json_list: json file
    :type transaction_json_list: list

    :param address: target address
    :type address: str

    :return: result_dict
    :rtype: dict

    :raises ValueError: There should be at least one customer in the specified location.

    .. note::
        1. Filters the transactions based on the address.
        2. Deserializes the filtered json file as a list of transaction objects.
        3. Iterates over the list and finds the age of each distinct customer.
        4. Computes the mean, median and std dev of the customer ages.
        5. Returns a dictionary of customer name and age.
    """

    filtered_list = list(filter(lambda transaction_obj: filter_by_address(transaction_obj, address),
                                transaction_json_list))

    # deserializing transaction_json_list into list of objects
    transaction_object_list = deserialize_transactions(filtered_list)

    try:
        # initiating new dict
        customer_age_dict = dict()

        # iterating over the list of objects
        for transaction in transaction_object_list:
            # getting customer name and age
            customer_name = transaction.customer.first_name
            # checks if the customer_name is not present as a key value
            if customer_name not in customer_age_dict.keys():
                # updating the dictionary with name as key and age as value
                customer_age_dict[customer_name] = transaction.customer.calculate_age()

        age_list = list(customer_age_dict.values())

        LOGGER.info("Youngest customer : %s", min(customer_age_dict, key=customer_age_dict.get))
        LOGGER.info("Eldest customer : %s", max(customer_age_dict, key=customer_age_dict.get))
        LOGGER.info("Mean customer age : %f", compute_mean(age_list))
        LOGGER.info("Median customer age : %f", compute_median(age_list))
        LOGGER.info("Std dev of customer age : %f", compute_standard_dev(age_list))

        return customer_age_dict

    except ValueError:
        LOGGER.error("There are no customers in %s.", address)
        raise
