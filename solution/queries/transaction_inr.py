# -*- coding: utf-8 -*-
"""
Transactions between two dates
----------------------------------
"""

import logging
from datetime import datetime
from solution.deserialize_objects import deserialize_transactions

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def filter_by_date(transaction, start_date, end_date):
    """
    filters the list of objects based on the manager name

    :param transaction: transaction object
    :type transaction: transaction

    :param start_date: start_date
    :type start_date: datetime.date

    :param end_date: end_date
    :type end_date: datetime.date

    :return: transaction object
    :rtype: transaction
    """
    return start_date < datetime.strptime(transaction["trans_date"], "%Y-%m-%d").date() < end_date


def calculate_inr_amount(transaction, exchange_rate):
    """
    converts trans_amount from USD to INR

    :return: trans_amount_inr
    :rtype: float
    """
    return transaction.trans_amount * exchange_rate


def get_transaction_inr(transaction_json_list, start_date, end_date, exchange_rate):
    """
    Returns the total transaction amount (in INR) between the given two dates.

    :param transaction_json_list: transaction_json_list
    :type transaction_json_list: list

    :param start_date: start_date
    :type start_date: datetime.date

    :param end_date: end_date
    :type end_date: datetime.date

    :param exchange_rate: exchange_rate
    :type exchange_rate: float

    :return: result_dict
    :rtype: dict

    .. note::
        1. Filters the json file based on start_date and end_date.
        2. Deserializes the filtered json file as a list of Transaction objects.
        3. Iterates over the list and adds-up the converted amount in each transaction.
        4. Returns the total transaction amount in INR.
    """

    # list of transactions between the start and end date
    filtered_list = list(
        filter(lambda transaction_obj: filter_by_date(transaction_obj, start_date, end_date), transaction_json_list))

    # deserializing the new json file into list of objects
    transaction_object_list = deserialize_transactions(filtered_list)

    # if the list is non-empty
    if transaction_object_list:
        # converting each transaction amount from usd to inr

        inr_amount_list = map(lambda transaction_obj: calculate_inr_amount(transaction_obj, exchange_rate),
                              transaction_object_list)
        # summing up all the inr_values in the new list
        total_trans_amount_inr = sum(inr_amount_list)
        result = round(total_trans_amount_inr, 2)

    else:
        LOGGER.error("No Transactions made between the given dates")
        result = 0

    return result
