# -*- coding: utf-8 -*-
"""
Transaction discrepancy in branches
---------------------------------------

**Discrepancy**

A discrepancy is a lack of agreement or balance. If there is a discrepancy between the money you earned and the
number on your paycheck, you should complain to your boss. There is a discrepancy when there is a difference between
two things that should be alike.

**Mean**

Mean is an arithmetic average of the data set and it can be calculated by dividing a sum of all the data points with
the number of data points in the data set. It is a point in a data set that is the average of all the data points we
have in a set.

**Median**

In statistics and probability theory, the median is the value separating the higher half from the lower half of a
data sample, a population, or a probability distribution. For a data set, it may be thought of as “the middle” value.

**Standard Deviation**

In statistics, the standard deviation is a measure of the amount of variation or dispersion of a set of values. A low
standard deviation indicates that the values tend to be close to the mean of the set, while a high standard deviation
indicates that the values are spread out over a wider range.

"""
import logging
from solution.queries.customer_age import compute_mean, compute_median, compute_standard_dev
from solution.deserialize_objects import deserialize_transactions

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def filter_branch_details(transaction, min_discount):
    """
    filters the transactions based on discount_percent

    :param transaction: transaction object
    :type transaction: transaction

    :param min_discount: minimum discount offered
    :type min_discount: float

    :return: transaction object
    :rtype: transaction
    """
    return transaction["discount_percent"] > min_discount


def get_branch_error(transaction_json_list, min_discount):
    """
    Returns a dictionary of error amounts in each branch

    :param transaction_json_list: json file
    :type transaction_json_list: list

    :param min_discount: minimum discount offered
    :type min_discount: float

    :return: branch_error_dict
    :rtype: dict

    :raises ValueError: There should be at least one transaction with discount_percent more than the specified value.

    .. note::
        1. Filters the json file based on min_discount.
        2. Deserializes the filtered list of dictionaries as a list of transaction objects.
        3. Iterates over the list and adds-up the transaction error in each branch.
        4. Finds the branches with the highest and lowest transaction errors.
        5. Finds the condition of the branches based on the year_opened.
        6. Computes the mean, median and std dev of the error amount of all 4 branches.
        7. Returns a dictionary of error amounts in each branch.
    """
    # filtering based on min_discount
    filtered_list = list(filter(lambda transaction_obj: filter_branch_details(transaction_obj, min_discount),
                                transaction_json_list))

    # deserializing the new json file into list of objects
    transaction_object_list = deserialize_transactions(filtered_list)

    # initialization
    branch_error_dict = dict()

    # iterating over the list of objects
    for transaction in transaction_object_list:
        # getting branch name and error
        branch_name = transaction.branch.name
        error = transaction.check_trans_amount()

        # adding up the error corresponding to each branch_name
        if branch_name in branch_error_dict:
            branch_error_dict[branch_name] += error
        else:
            branch_error_dict[branch_name] = error

    try:
        # getting the branches with highest and lowest errors
        max_error_branch = max(branch_error_dict, key=branch_error_dict.get)
        max_error_branch_condition = str()
        min_error_branch = min(branch_error_dict, key=branch_error_dict.get)
        min_error_branch_condition = str()

        # getting the condition of the branch
        for transaction in transaction_object_list:
            if transaction.branch.name == max_error_branch:
                max_error_branch_condition = transaction.branch.get_branch_condition()
                break

        # getting the condition of the branch
        for transaction in transaction_object_list:
            if transaction.branch.name == min_error_branch:
                min_error_branch_condition = transaction.branch.get_branch_condition()
                break

        # displaying results
        LOGGER.info("Branch with highest error : %s", max_error_branch)
        LOGGER.info("       Branch condition : %s", max_error_branch_condition)
        LOGGER.info("Branch with least error : %s", min_error_branch)
        LOGGER.info("       Branch condition : %s", min_error_branch_condition)

        # computing error amount stats
        error_values_list = list(branch_error_dict.values())
        LOGGER.info("Number of branches : %d", len(branch_error_dict))
        LOGGER.info("Mean error amount of branches ($) : %f", compute_mean(error_values_list))
        LOGGER.info("Median error amount ($) : %f", compute_median(error_values_list))
        LOGGER.info("Std dev of error amount ($) : %f", compute_standard_dev(error_values_list))

        return branch_error_dict

    except ValueError:
        LOGGER.error("There are no transactions with discount_percent more than %d.", min_discount)
        raise
