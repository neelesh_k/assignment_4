# -*- coding: utf-8 -*-
"""
Renaming a Manager
--------------------

Rename a Manager in the json file

"""
import json
import logging
from solution.class_schema.transaction_schema import TransactionSchema
from solution.deserialize_objects import deserialize_transactions

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def filter_by_manager_name(transaction, old_first_name, old_last_name):
    """
    filters the list of objects based on the manager name

    :param transaction: transaction dict
    :type transaction: dict

    :param old_first_name: first name of manager
    :type old_first_name: str

    :param old_last_name: last name of manager
    :type old_last_name: str

    :return: transaction dict
    :rtype: dict
    """
    # getting the first and last name of the manager
    manager_first_name = transaction["staff"]["manager"]["first_name"]
    manager_last_name = transaction["staff"]["manager"]["last_name"]
    return manager_first_name == old_first_name and manager_last_name == old_last_name


def rename_manager(transaction_json_list, old_first_name, old_last_name, new_first_name, new_last_name):
    """
    Renaming the first and last name of the Manager

    :param transaction_json_list: list of transaction dictionaries
    :type transaction_json_list: list

    :param old_first_name: existing first name
    :type old_first_name: str

    :param old_last_name: existing last name
    :type old_last_name: str

    :param new_first_name: new first name
    :type new_first_name: str

    :param new_last_name: new last name
    :type new_last_name: str

    :return: json_data
    :rtype: list

    .. note::
        1. Deserializing the list of transaction dictionaries to list of transaction objects.
        2. Iterating the list to get the manager name in each transaction.
        3. Renaming the manager name using method operations.
        4. Serializing  the list of transaction objects to list of transaction dictionaries.
    """
    # deserializing the new json file into list of objects
    filtered_list = list(
        filter(lambda transaction_dict: filter_by_manager_name(transaction_dict, old_first_name, old_last_name),
               transaction_json_list))

    transaction_object_list = deserialize_transactions(filtered_list)

    if filtered_list:
        # iterating the given list of json values
        for transaction in transaction_object_list:
            # performing method operation
            transaction.staff.manager.first_name = new_first_name
            transaction.staff.manager.last_name = new_last_name

            LOGGER.debug("Renamed in transaction having id: %d", transaction.trans_id)

        # serializing
        schema = TransactionSchema()
        json_data = schema.dump(transaction_object_list, many=True)
        # dumping to a json file
        with open(r"results/renamed_transactions.json", "w") as json_file:
            json.dump(json_data, json_file)
        LOGGER.info("Created renamed_transactions.json file")
        result = json_data

    else:
        LOGGER.error("No manager named %s %s", old_first_name, old_last_name)
        result = transaction_json_list

    return result
