# -*- coding: utf-8 -*-
"""
Manager class
--------------
"""
import logging
from solution.class_design.staff import Staff

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class Manager(Staff):
    """
    Manager class docstring

    :ivar manager_id: manager's id
    :vartype manager_id: int

    :ivar first_name: person's first_name
    :vartype first_name: str

    :ivar last_name: person's last_name
    :vartype last_name: str

    :ivar level: employee's level
    :vartype level: int

    :ivar salary: salary
    :vartype salary: int

    :ivar dob: person's dob
    :vartype dob: datetime.date

    :ivar email: person's email
    :vartype email: str

    :ivar phone: person's phone
    :vartype phone: str

    :ivar address: person's address
    :vartype address: str

    """

    def __init__(self, manager_id, first_name, last_name, level, salary, dob, email, phone, address,
                 manager=None):
        """
        creates an instance of the class

        :param manager_id:manager's id
        :type manager_id:int

        :param first_name:person's first_name
        :type first_name:str

        :param last_name:person's last_name
        :type last_name:str

        :param level: employee's level
        :type level: int

        :param salary: salary
        :type salary: int

        :param dob:person's dob
        :type dob:datetime.date

        :param email:person's email
        :type email:str

        :param phone:person's phone
        :type phone:str

        :param address:person's address
        :type address:str

        """

        super().__init__(manager_id, first_name, last_name, level, salary, manager, dob, email, phone, address)
        self.manager_id = manager_id
        self.first_name = first_name
        self.last_name = last_name
        self.level = level
        self.salary = salary
        self.dob = dob
        self.email = email
        self.phone = phone
        self.address = address
