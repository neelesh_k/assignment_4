# -*- coding: utf-8 -*-
"""
Person class
--------------
"""
import logging
from datetime import date

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class Person:
    """
    Person class docstring

    :ivar person_id: person's id
    :vartype person_id: int

    :ivar first_name: person's first_name
    :vartype first_name: str

    :ivar last_name: person's last_name
    :vartype last_name: str

    :ivar dob: person's dob
    :vartype dob: datetime.date

    :ivar email: person's email
    :vartype email: str

    :ivar phone: person's phone
    :vartype phone: str

    :ivar address: person's address
    :vartype address: str
    """

    def __init__(self, person_id, first_name, last_name, dob, email, phone, address):
        """
        creates an instance of the class

        :param person_id:person's id
        :type person_id:int

        :param first_name:person's first_name
        :type first_name:str

        :param last_name:person's last_name
        :type last_name:str

        :param dob:person's dob
        :type dob:datetime.date

        :param email:person's email
        :type email:str

        :param phone:person's phone
        :type phone:str

        :param address:person's address
        :type address:str

        """
        self.person_id = person_id
        self.first_name = first_name
        self.last_name = last_name
        self.dob = dob
        self.email = email
        self.phone = phone
        self.address = address

    def calculate_age(self):
        """
        returns the age of a given person

        :return: age
        :rtype: int
        """
        birth_date = self.dob
        end_date = date.today()

        age_year = end_date.year - birth_date.year

        return age_year

    def calculate_exact_age(self):
        """
        returns the age of a given person

        :return: a tuple of

                *  age_year: age_year

                * age_month: age_month

                * age_day: age_day

        :rtype: tuple
        """
        birth_date = self.dob
        end_date = date.today()

        age_year = end_date.year - birth_date.year
        age_month = abs(end_date.month - birth_date.month)
        age_day = abs(end_date.day - birth_date.day)

        age = "{} years, {} months and {} days".format(age_year, age_month, age_day)

        return age

    def change_first_name(self, new_name):
        """
        changes the existing value of the variable to a new value

        :param new_name: new variable value
        :type new_name: str

        :return: None
        :rtype: None
        """
        self.first_name = new_name

    def change_last_name(self, new_name):
        """
        changes the existing value of the variable to a new value

        :param new_name: new variable value
        :type new_name: str

        :return: None
        :rtype: None
        """
        self.last_name = new_name

    def change_dob(self, new_dob):
        """
        changes the existing value of the variable to a new value

        :param new_dob: new variable value
        :type new_dob: datetime.date

        :return: None
        :rtype: None
        """
        self.dob = new_dob

    def change_email(self, new_email):
        """
        changes the existing value of the variable to a new value

        :param new_email: new variable value
        :type new_email: str

        :return: None
        :rtype: None
        """
        self.email = new_email

    def change_phone(self, new_phone):
        """
        changes the existing value of the variable to a new value

        :param new_phone: new variable value
        :type new_phone: int

        :return: None
        :rtype: None
        """
        self.phone = new_phone

    def change_address(self, new_address):
        """
        changes the existing value of the variable to a new value

        :param new_address: new variable value
        :type new_address: str

        :return: None
        :rtype: None
        """
        self.address = new_address
