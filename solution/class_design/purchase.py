# -*- coding: utf-8 -*-
"""
Purchase class
--------------
"""
import logging

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class Purchase:
    """
    Purchase class docstring

    :ivar purchase_id: purchase_id
    :vartype purchase_id: int

    :ivar product: product object
    :vartype product: Product

    :ivar num_units: num_units
    :vartype num_units: int

    """

    def __init__(self, purchase_id, product, num_units):
        """
        creates an instance of the class

        :param purchase_id: purchase_id
        :type purchase_id: int

        :param product: product object
        :type product: Product

        :param num_units: num_units
        :type num_units: int

        """
        self.purchase_id = purchase_id
        self.product = product
        self.num_units = num_units



    def change_num_units(self, new_num_units):
        """
        changes the existing value of the variable to a new value

        :param new_num_units: new variable value
        :type new_num_units: str

        :return: None
        :rtype: None
        """
        self.num_units = new_num_units
