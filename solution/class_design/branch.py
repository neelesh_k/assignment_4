# -*- coding: utf-8 -*-
"""
Branch class
--------------
"""

import logging
from datetime import date

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class Branch:
    """
    Branch class docstring

    :ivar branch_id: branch's id
    :vartype branch_id: int

    :ivar name: name
    :vartype name: str

    :ivar address: address
    :vartype address: str

    :ivar pincode: pincode
    :vartype pincode: pincode

    :ivar year_opened: year_opened
    :vartype year_opened: int

    """

    def __init__(self, branch_id, name, address, pincode, year_opened):
        """
        creates an instance of the class

        :param branch_id: branch's id
        :type  branch_id: int

        :param name: branch's name
        :type  name: str

        :param address: branch's address
        :type  address: str

        :param pincode: branch's pincode
        :type  pincode: str

        :param year_opened: year of opening
        :type  year_opened: int

        :return: None
        :rtype: None
        """
        self.branch_id = branch_id
        self.name = name
        self.address = address
        self.pincode = pincode
        self.year_opened = year_opened

    def get_branch_condition(self):
        """
        returns the condition of a given branch

        :return: condition
        :rtype: str
        """
        start_date = self.year_opened
        end_date = date.today()

        branch_age = end_date.year - start_date

        if branch_age <= 10:
            condition = "New - No need for maintenance."
        elif 10 < branch_age <= 20:
            condition = "Moderate - Need maintenance."
        elif 20 < branch_age <= 30:
            condition = "Old - Check for faulty machines."
        else:
            condition = "Very old - Replace machines."

        return condition

    def change_branch_id(self, new_branch_id):
        """
        changes the existing value of the variable to a new value

        :param new_branch_id: new variable value
        :type new_branch_id: int

        :return: None
        :rtype: None
        """
        self.branch_id = new_branch_id

    def change_name(self, new_name):
        """
        changes the existing value of the variable to a new value

        :param new_name: new variable value
        :type new_name: str

        :return: None
        :rtype: None
        """
        self.name = new_name

    def change_address(self, new_address):
        """
        changes the existing value of the variable to a new value

        :param new_address: new variable value
        :type new_address: str

        :return: None
        :rtype: None
        """
        self.address = new_address

    def change_pincode(self, new_pincode):
        """
        changes the existing value of the variable to a new value

        :param new_pincode: new variable value
        :type new_pincode: str

        :return: None
        :rtype: None
        """
        self.pincode = new_pincode

    def change_year_opened(self, new_year_opened):
        """
        changes the existing value of the variable to a new value

        :param new_year_opened: new variable value
        :type new_year_opened: str

        :return: None
        :rtype: None
        """
        self.year_opened = new_year_opened
