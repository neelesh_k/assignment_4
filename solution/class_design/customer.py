# -*- coding: utf-8 -*-
"""
Customer class
--------------
"""
import logging
from solution.class_design.person import Person

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class Customer(Person):
    """
    Customer class docstring

    :ivar customer_id: customer's id
    :vartype customer_id: int

    :ivar first_name: person's first_name
    :vartype first_name: str

    :ivar last_name: person's last_name
    :vartype last_name: str

    :ivar dob: person's dob
    :vartype dob: datetime.date

    :ivar email: person's email
    :vartype email: str

    :ivar phone: person's phone
    :vartype phone: str

    :ivar address: person's address
    :vartype address: str

    """

    def __init__(self, customer_id, first_name, last_name, dob, email, phone, address):
        """
        creates an instance of the class

        :param customer_id: customer's id
        :type customer_id: int

        :param first_name: person's first_name
        :type first_name: str

        :param last_name: person's last_name
        :type last_name: str

        :param dob: person's dob
        :type dob: datetime.date

        :param email: person's email
        :type email: str

        :param phone: person's phone
        :type phone: str

        :param address: person's address
        :type address: str

        """
        super().__init__(customer_id, first_name, last_name, dob, email, phone, address)
        self.customer_id = customer_id
        self.first_name = first_name
        self.last_name = last_name
        self.dob = dob
        self.email = email
        self.phone = phone
        self.address = address
