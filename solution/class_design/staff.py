# -*- coding: utf-8 -*-
"""
Staff class
--------------
"""
import logging
from solution.class_design.person import Person

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class Staff(Person):
    """
    Staff class docstring

    :ivar staff_id: staff's id
    :vartype staff_id: int

    :ivar first_name: person's first_name
    :vartype first_name: str

    :ivar last_name: person's last_name
    :vartype last_name: str

    :ivar level: employee's level
    :vartype level: int

    :ivar salary: salary
    :vartype salary: int

    :ivar manager: manager object
    :vartype manager: Manager

    :ivar dob: person's dob
    :vartype dob: datetime.date

    :ivar email: person's email
    :vartype email: str

    :ivar phone: person's phone
    :vartype phone: str

    :ivar address: person's address
    :vartype address: str

    """

    def __init__(self, staff_id, first_name, last_name, level, salary, manager, dob, email, phone, address):
        """
        creates an instance of the class

        :param staff_id:staff's id
        :type staff_id:int

        :param first_name:person's first_name
        :type first_name:str

        :param last_name:person's last_name
        :type last_name:str

        :param level: employee's level
        :type level: int

        :param salary: salary
        :type salary: int

        :param manager: manager object
        :type manager: Manager

        :param dob:person's dob
        :type dob:datetime.date

        :param email:person's email
        :type email:str

        :param phone:person's phone
        :type phone:str

        :param address:person's address
        :type address:str

        """
        super().__init__(staff_id, first_name, last_name, dob, email, phone, address)
        self.staff_id = staff_id
        self.first_name = first_name
        self.last_name = last_name
        self.level = level
        self.salary = salary
        self.manager = manager
        self.dob = dob
        self.email = email
        self.phone = phone
        self.address = address

    def promote_staff(self):
        """
        increases the level of the staff by 1 and salary based on the level

        :return: None
        :rtype: None
        """
        self.level += 1
        self.salary = self.salary + (self.salary * (self.level / 10))
