# -*- coding: utf-8 -*-
"""
Transaction class
-------------------
"""
import logging

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class Transaction:
    """
    Transaction class docstring

    :ivar trans_id: trans_id
    :vartype trans_id: int

    :ivar customer: customer object
    :vartype customer: Customer

    :ivar staff: staff object
    :vartype staff: Staff

    :ivar branch: branch object
    :vartype branch: Branch

    :ivar trans_amount: trans_amount
    :vartype trans_amount: float

    :ivar trans_date: trans_date
    :vartype trans_date: datetime.date

    :ivar discount_percent: discount_percent
    :vartype discount_percent: float
    """

    def __init__(self, trans_id, customer, staff, branch, purchases_list, trans_amount, trans_date, discount_percent):
        """
        creates an instance of the class

        :param trans_id:trans_id
        :type trans_id: int

        :param customer:customer object
        :type customer: Customer

        :param staff:staff object
        :type staff: Staff

        :param branch:branch object
        :type branch: Branch

        :param purchases_list: purchases_list
        :type purchases_list: list

        :param trans_amount:trans_amount
        :type trans_amount: float

        :param trans_date:trans_date
        :type trans_date: datetime.date

        :param discount_percent:discount_percent
        :type discount_percent: float

        """
        self.trans_id = trans_id
        self.customer = customer
        self.staff = staff
        self.branch = branch
        self.purchases_list = purchases_list
        self.trans_amount = trans_amount
        self.trans_date = trans_date
        self.discount_percent = discount_percent

    def check_trans_amount(self):
        """
        returns the difference between the actual transaction amount and the entered transaction amount.

        :return: result
        :rtype: float
        """
        total_amount = 0

        for purchase in self.purchases_list:
            amount_without_discount = purchase.product.price_usd * purchase.num_units
            amount_with_discount = amount_without_discount * (100 - self.discount_percent) / 100
            total_amount += amount_with_discount

        if total_amount == self.trans_amount:
            result = 0
        else:
            error = abs(total_amount - self.trans_amount)
            result = round(error, 2)

        return result
