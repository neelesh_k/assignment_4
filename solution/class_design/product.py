# -*- coding: utf-8 -*-
"""
Product class
--------------
"""
import logging

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class Product:
    """
    class docstring

    :ivar product_id: product_id
    :vartype product_id: int

    :ivar name: product_name
    :vartype name: str

    :ivar category: category
    :vartype category: str

    :ivar manufacturer: manufacturer
    :vartype manufacturer: str

    :ivar price_usd: price_usd
    :vartype price_usd: float

    :ivar year_manufactured: year_manufactured
    :vartype year_manufactured: int
    """

    def __init__(self, product_id, name, category, manufacturer, price_usd, year_manufactured):
        """
        creates an instance of the class

        :param product_id:product_id
        :type product_id: int

        :param name:product_name
        :type name:str

        :param category:category
        :type category:str

        :param manufacturer:manufacturer
        :type manufacturer:str

        :param price_usd:price_usd
        :type price_usd:float

        :param year_manufactured:year_manufactured
        :type year_manufactured:int


        :return None

        """

        self.product_id = product_id
        self.name = name
        self.category = category
        self.manufacturer = manufacturer
        self.price_usd = price_usd
        self.year_manufactured = year_manufactured

    def calculate_inr_amount(self):
        """
        converts price_usd from USD to INR

        :return: price_usd_inr
        """
        price_usd_inr = self.price_usd * 74.44

        return price_usd_inr

    def change_name(self, new_name):
        """
        changes the existing value of the variable to a new value

        :param new_name: new variable value
        :type new_name: str

        :return: None
        :rtype: None
        """
        self.name = new_name

    def change_category(self, new_category):
        """
        changes the existing value of the variable to a new value

        :param new_category: new variable value
        :type new_category: str

        :return: None
        :rtype: None
        """
        self.category = new_category

    def change_manufacturer(self, new_manufacturer):
        """
        changes the existing value of the variable to a new value

        :param new_manufacturer: new variable value
        :type new_manufacturer: str

        :return: None
        :rtype: None
        """
        self.manufacturer = new_manufacturer

    def change_price_usd(self, new_price_usd):
        """
        changes the existing value of the variable to a new value

        :param new_price_usd: new variable value
        :type new_price_usd: str


        :return: None
        :rtype: None
        """
        self.price_usd = new_price_usd

    def change_year_manufactured(self, new_year_manufactured):
        """
        changes the existing value of the variable to a new value

        :param new_year_manufactured: new variable value
        :type new_year_manufactured: str

        :return: None
        :rtype: None
        """
        self.year_manufactured = new_year_manufactured
