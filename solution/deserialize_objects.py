# -*- coding: utf-8 -*-
"""
Deserializing Objects (“Loading”)
-----------------------------------

The reverse of the dump method is load, which validates and deserializes an input dictionary to an application-level
data structure.

"""
import logging
from solution.class_schema.branch_schema import BranchSchema
from solution.class_schema.customer_schema import CustomerSchema
from solution.class_schema.product_schema import ProductSchema
from solution.class_schema.purchase_schema import PurchaseSchema
from solution.class_schema.staff_schema import StaffSchema
from solution.class_schema.transaction_schema import TransactionSchema

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def deserialize_branches(json_data_list):
    """
    deserializes json files

    :param json_data_list: json_data_list
    :type json_data_list: list

    :return: object_list
    :rtype: list
    """
    schema = BranchSchema()
    branches_object_list = schema.load(json_data_list, many=True)

    return branches_object_list


def deserialize_customers(json_data_list):
    """
    deserializes json files

    :param json_data_list: json_data_list
    :type json_data_list: list

    :return: object_list
    :rtype: list
    """

    schema = CustomerSchema()
    customers_object_list = schema.load(json_data_list, many=True)

    return customers_object_list


def deserialize_products(json_data_list):
    """
    deserializes json files

    :param json_data_list: json_data_list
    :type json_data_list: list

    :return: object_list
    :rtype: list
    """
    schema = ProductSchema()
    products_object_list = schema.load(json_data_list, many=True)

    return products_object_list


def deserialize_purchases(json_data_list):
    """
    deserializes json files

    :param json_data_list: json_data_list
    :type json_data_list: list

    :return: object_list
    :rtype: list
    """
    schema = PurchaseSchema()
    purchases_object_list = schema.load(json_data_list, many=True)

    return purchases_object_list


def deserialize_staffs(json_data_list):
    """
    deserializes json files

    :param json_data_list: json_data_list
    :type json_data_list: list

    :return: object_list
    :rtype: list
    """
    schema = StaffSchema()
    staffs_object_list = schema.load(json_data_list, many=True)

    return staffs_object_list


def deserialize_transactions(json_data_list):
    """
    deserializes json files

    :param json_data_list: json_data_list
    :type json_data_list: list

    :return: object_list
    :rtype: list
    """
    schema = TransactionSchema()
    transactions_object_list = schema.load(json_data_list, many=True)

    return transactions_object_list
