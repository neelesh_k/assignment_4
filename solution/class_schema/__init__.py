# -*- coding: utf-8 -*-
"""

The main component of Marshmallow is a Schema. A schema defines the rules that guides deserialization, called load,
and serialization, called dump. It allows us to define the fields that will be loaded or dumped, add requirements on
the fields, like validation or required.

"""

import logging

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)
