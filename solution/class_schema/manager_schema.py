# -*- coding: utf-8 -*-
"""
Manager schema
--------------
"""
import logging
from marshmallow import Schema, fields, post_load
from solution.class_schema.custom_fields import PhoneNumber
from solution.class_design.manager import Manager

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class ManagerSchema(Schema):
    """
    Schema class for Manager
    """
    manager_id = fields.Integer(required=True,
                                error_messages={
                                    "required": "Id is required. Enter id-number to continue",
                                    "null": "Id field cannot be null",
                                    "invalid": "Enter a valid integer"})

    first_name = fields.Str(required=True,
                            error_messages={
                                "required": "First name is required. Enter first-name to continue",
                                "null": "String field cannot be null",
                                "invalid": "Enter a valid string"})

    last_name = fields.Str(required=True,
                           error_messages={
                               "required": "Last name is required. Enter first-name to continue",
                               "null": "String field cannot be null",
                               "invalid": "Enter a valid string"})

    level = fields.Integer(required=True,
                           error_messages={"required": "level is required. Enter level to continue",
                                           "null": "level field cannot be null",
                                           "invalid": "Enter a valid integer"})

    salary = fields.Integer(required=True,
                            error_messages={"required": "salary is required. Enter level to continue",
                                            "null": "salary field cannot be null",
                                            "invalid": "Enter a valid integer"})

    dob = fields.Date(required=True,
                      error_messages={"required": "dob is required. Enter dob to continue",
                                      "null": "dob field cannot be null",
                                      "invalid": "Enter a valid dob"})
    email = fields.Email(required=True,
                         error_messages={
                             "required": "E-mail is required. Enter email-id to continue",
                             "null": "E-mail field cannot be null",
                             "invalid": "Enter a valid e-mail"})

    phone = PhoneNumber(required=True,
                        error_messages={"required": "Phone number is required. Enter phone number to continue",
                                        "null": "Phone number field cannot be null",
                                        "invalid": "Enter a valid phone number"})

    address = fields.Str(required=True,
                         error_messages={"required": "address is required. Enter address to continue",
                                         "null": "String field cannot be null",
                                         "invalid": "Enter a valid address"})

    @post_load
    def instantiate_staff(self, data, **kwargs):
        """
        instantiates class object

        :param data: json data
        :type data: dict

        :return: manager object
        :rtype: Manager
        """
        return Manager(**data)
