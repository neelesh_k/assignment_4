# -*- coding: utf-8 -*-
"""
Branch schema
--------------
"""
import logging
from marshmallow import Schema, fields, post_load, validates, ValidationError
from solution.class_schema.custom_fields import PinCode
from solution.class_design.branch import Branch

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class BranchSchema(Schema):
    """
    Schema class for Branch
    """

    branch_id = fields.Integer(required=True,
                               error_messages={
                                   "required": "branch_id is required. Enter branch_id to continue",
                                   "null": "Integer field cannot be null",
                                   "invalid": "Enter a valid integer"})

    name = fields.Str(required=True,
                      error_messages={
                          "required": "name is required. Enter name to continue",
                          "null": "String field cannot be null",
                          "invalid": "Enter a valid string"})

    address = fields.Str(required=True,
                         error_messages={
                             "required": "Address is required. Enter address to continue",
                             "null": "String field cannot be null",
                             "invalid": "Enter a valid String."})

    pincode = PinCode(required=True,
                      error_messages={
                          "required": "Pincode is required. Enter pincode to continue",
                          "null": "Pincode field cannot be null",
                          "invalid": "Enter a valid six-digit number."})

    year_opened = fields.Integer(required=True,
                                 error_messages={
                                     "required": "Year is required. Enter Year to continue",
                                     "null": "Integer field cannot be null",
                                     "invalid": "Enter a valid integer"})

    @validates("year_opened")
    def validate_year_opened(self, value):
        """
        validates year

        :param value: year
        :type value: int

        :return: None
        :rtype: None
        """
        if value < 0:
            raise ValidationError("Year must be greater than 0.")
        if value > 2021:
            raise ValidationError("Year must not be greater than 2021.")

    @post_load
    def instantiate_branch(self, data, **kwargs):
        """
        instantiates class object

        :param data: json data
        :type data: dict

        :return: branch object
        :rtype: Branch
        """
        return Branch(**data)
