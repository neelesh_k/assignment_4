# -*- coding: utf-8 -*-
"""
Custom Fields
--------------

To create a custom field class, create a subclass of marshmallow.fields.Field and
implement its _serialize and/or _deserialize methods.
"""
import logging
from marshmallow import fields, ValidationError

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class PinCode(fields.Field):
    """
    PinCode Field

    """

    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return ""
        return "".join(str(d) for d in value)

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            return [int(c) for c in value]
        except ValueError as error:
            raise ValidationError("Pin codes must contain only digits.") from error


class PhoneNumber(fields.Field):
    """
    PhoneNumber Field

    """

    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return ""
        return "".join(str(d) for d in value)

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            return [int(c) for c in value]
        except ValueError as error:
            raise ValidationError("Phone number must contain only digits.") from error
