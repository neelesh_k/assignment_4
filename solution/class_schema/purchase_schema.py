# -*- coding: utf-8 -*-
"""
Purchase schema
-----------------
"""
import logging
from marshmallow import Schema, fields, post_load
from solution.class_design.purchase import Purchase
from solution.class_schema.product_schema import ProductSchema

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class PurchaseSchema(Schema):
    """
    Schema class for Purchase
    """
    purchase_id = fields.Integer(required=True,
                                 error_messages={"required": "purchase_id is required. Enter purchase_id to continue",
                                                 "null": "Integer field cannot be null",
                                                 "invalid": "Enter a valid Integer"})

    product = fields.Nested(ProductSchema)

    num_units = fields.Integer(required=True,
                               error_messages={"required": "num_units is required. Enter num_units to continue",
                                               "null": "Integer field cannot be null",
                                               "invalid": "Enter a valid Integer"})

    @post_load
    def instantiate_purchase(self, data, **kwargs):
        """
        instantiates class object

        :param data: json data
        :type data: dict

        :return: purchase object
        :rtype: Purchase
        """
        return Purchase(**data)
