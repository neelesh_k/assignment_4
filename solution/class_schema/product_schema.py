# -*- coding: utf-8 -*-
"""
Product schema
--------------
"""
import logging
from marshmallow import Schema, fields, post_load
from solution.class_design.product import Product

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class ProductSchema(Schema):
    """
    Schema class for Product
    """
    product_id = fields.Integer(required=True,
                                error_messages={"required": "product_id is required. Enter product_id to continue",
                                                "null": "product_id field cannot be null",
                                                "invalid": "Enter a valid product_id"})
    name = fields.Str(required=True,
                      error_messages={"required": "product name is required. Enter product name to continue",
                                      "null": "String field cannot be null",
                                      "invalid": "Enter a valid String"})
    category = fields.Str(required=True,
                          error_messages={"required": "category is required. Enter category to continue",
                                          "null": "String field cannot be null",
                                          "invalid": "Enter a valid String"})
    manufacturer = fields.Str(required=True,
                              error_messages={"required": "manufacturer is required. Enter manufacturer to continue",
                                              "null": "String field cannot be null",
                                              "invalid": "Enter a valid String"})

    price_usd = fields.Float(required=True,
                             error_messages={"required": "price_us is required. Enter price_us to continue",
                                             "null": "price_us field cannot be null",
                                             "invalid": "Enter a valid price_us"})

    year_manufactured = fields.Integer(required=True,
                                       error_messages={
                                           "required": "year_manufactured is required. Enter year to continue",
                                           "null": "Integer field cannot be null",
                                           "invalid": "Enter a valid Integer"})

    @post_load
    def instantiate_product(self, data, **kwargs):
        """
        instantiates class object

        :param data: json data
        :type data: dict

        :return: product object
        :rtype: Product
        """
        return Product(**data)
