# -*- coding: utf-8 -*-
"""
Staff schema
--------------
"""
import logging
from marshmallow import Schema, fields, post_load
from solution.class_design.staff import Staff
from solution.class_schema.custom_fields import PhoneNumber

__author__ = "neelesh@gyandata.com"

# Get logger
from solution.class_schema.manager_schema import ManagerSchema

LOGGER = logging.getLogger(name=__name__)


class StaffSchema(Schema):
    """
    Schema class for Staff
    """
    staff_id = fields.Integer(required=True,
                              error_messages={"required": "staff_id is required. Enter staff_id to continue",
                                              "null": "staff_id field cannot be null",
                                              "invalid": "Enter a valid Integer"})

    first_name = fields.Str(required=True,
                            error_messages={"required": "first_name is required. Enter first_name to continue",
                                            "null": "String field cannot be null",
                                            "invalid": "Enter a valid String"})

    last_name = fields.Str(required=True,
                           error_messages={"required": "last_name is required. Enter last_name to continue",
                                           "null": "String field cannot be null",
                                           "invalid": "Enter a valid String"})

    level = fields.Integer(required=True,
                           error_messages={"required": "level is required. Enter level to continue",
                                           "null": "Integer field cannot be null",
                                           "invalid": "Enter a valid integer"})

    salary = fields.Integer(required=True,
                            error_messages={"required": "salary is required. Enter level to continue",
                                            "null": "Integer field cannot be null",
                                            "invalid": "Enter a valid integer"})

    manager = fields.Nested(ManagerSchema)

    email = fields.Email(required=True,
                         error_messages={"required": "email is required. Enter email to continue",
                                         "null": "email field cannot be null",
                                         "invalid": "Enter a valid email"})

    dob = fields.Date(required=True,
                      error_messages={"required": "dob is required. Enter dob to continue",
                                      "null": "dob field cannot be null",
                                      "invalid": "Enter a valid dob"})

    phone = PhoneNumber(required=True,
                        error_messages={"required": "phone is required. Enter phone to continue",
                                        "null": "phone field cannot be null",
                                        "invalid": "Enter a valid phone"})

    address = fields.Str(required=True,
                         error_messages={"required": "address is required. Enter address to continue",
                                         "null": "String field cannot be null",
                                         "invalid": "Enter a valid String"})

    @post_load
    def instantiate_staff(self, data, **kwargs):
        """
        instantiates class object

        :param data: json data
        :type data: dict

        :return: staff object
        :rtype: Staff
        """
        return Staff(**data)
