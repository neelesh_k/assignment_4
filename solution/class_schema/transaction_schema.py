# -*- coding: utf-8 -*-
"""
Transaction schema
--------------------
"""
import logging
from marshmallow import Schema, fields, post_load, validates, ValidationError
from solution.class_design.transaction import Transaction
from solution.class_schema.branch_schema import BranchSchema
from solution.class_schema.customer_schema import CustomerSchema
from solution.class_schema.purchase_schema import PurchaseSchema
from solution.class_schema.staff_schema import StaffSchema

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class TransactionSchema(Schema):
    """
    Schema class for Transaction
    """
    trans_id = fields.Integer(required=True,
                              error_messages={"required": "trans_id is required. Enter trans_id to continue",
                                              "null": "trans_id field cannot be null",
                                              "invalid": "Enter a valid Integer"})

    customer = fields.Nested(CustomerSchema)
    staff = fields.Nested(StaffSchema)
    branch = fields.Nested(BranchSchema)
    purchases_list = fields.List(fields.Nested(PurchaseSchema))

    trans_amount = fields.Integer(required=True,
                                  error_messages={
                                      "required": "trans_amount is required. Enter trans_amount to continue",
                                      "null": "trans_amount field cannot be null",
                                      "invalid": "Enter a valid Integer"})
    trans_date = fields.Date(required=True,
                             error_messages={"required": "trans_date is required. Enter trans_date to continue",
                                             "null": "trans_date field cannot be null",
                                             "invalid": "Enter a valid trans_date"})

    discount_percent = fields.Float(required=True,
                                    error_messages={
                                        "required": "discount_percent is required. Enter discount_percent to continue",
                                        "null": "discount_percent field cannot be null",
                                        "invalid": "Enter a valid discount_percent"})

    @post_load
    def instantiate_transaction(self, data, **kwargs):
        """
        instantiates class object

        :param data: json data
        :type data: dict

        :return: transaction object
        :rtype: Transaction
        """
        return Transaction(**data)

    @validates("trans_amount")
    def validate_trans_amount(self, value):
        """
        validates trans_amount

        :param value: trans_amount
        :type value: int

        :return: None
        :rtype: None
        """
        if value < 0:
            raise ValidationError("Transaction amount cannot be negative.")

    @validates("discount_percent")
    def validate_discount_percent(self, value):
        """
        validates discount_percent

        :param value: discount_percent
        :type value: float

        :return: None
        :rtype: None
        """
        if value < 0:
            raise ValidationError("Discount percent cannot be negative.")
