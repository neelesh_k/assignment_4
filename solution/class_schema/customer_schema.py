# -*- coding: utf-8 -*-
"""
Customer schema
------------------
"""
import logging
from marshmallow import Schema, fields, post_load, validates, ValidationError
from solution.class_schema.custom_fields import PhoneNumber
from solution.class_design.customer import Customer

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


class CustomerSchema(Schema):
    """
    Schema class for Customer
    """
    customer_id = fields.Integer(required=True,
                                 error_messages={
                                     "required": "customer_id is required. Enter id-number to continue",
                                     "null": "Id field cannot be null",
                                     "invalid": "Enter a valid integer"})

    first_name = fields.Str(required=True,
                            error_messages={
                                "required": "Customer's First name is required. Enter first-name to continue",
                                "null": "String field cannot be null",
                                "invalid": "Enter a valid string"})

    last_name = fields.Str(required=True,
                           error_messages={
                                "required": "Customer's Last name is required. Enter last-name to continue",
                                "null": "String field cannot be null",
                                "invalid": "Enter a valid string"})

    dob = fields.Date(required=True,
                      error_messages={"invalid": "Enter a valid date"})

    email = fields.Email(required=True,
                         error_messages={
                             "required": "Customer's E-mail is required. Enter email-id to continue",
                             "null": "E-mail field cannot be null",
                             "invalid": "Enter a valid e-mail"})

    phone = PhoneNumber(required=True,
                        error_messages={"invalid": "Enter a valid phone-number"})

    address = fields.Str(required=True,
                         error_messages={"invalid": "Enter a valid address"})

    @validates("customer_id")
    def validate_customer_id(self, value):
        """
        validates customer_id

        :param value: customer_id
        :type value: int

        :return: None
        :rtype: None
        """
        if value < 0:
            raise ValidationError("Customer_id must be greater than 0.")

    @post_load
    def instantiate_customer(self, data, **kwargs):
        """
        instantiates class object

        :param data: json data
        :type data: dict

        :return: customer object
        :rtype: Customer
        """
        return Customer(**data)
