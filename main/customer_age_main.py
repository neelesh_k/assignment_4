# -*- coding: utf-8 -*-
"""
Rename the branch that generated the highest revenue
"""
import json
import logging
from helper import initialize_logger, initialize_command_args_customer_age
from solution.queries.customer_age import get_customer_stats
from solution.object_creation.transaction_objects import create_objects

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def customer_age_main():
    # construct objects and serialize as transactions.json file
    create_objects()

    address = initialize_command_args_customer_age()

    # loading transactions.json file into a list
    with open("transactions.json") as json_file:
        transaction_json_list = json.load(json_file)

    # Get the oldest and the youngest customer
    LOGGER.info(get_customer_stats(transaction_json_list, address))


if __name__ == '__main__':
    # initialize logger
    initialize_logger()

    # main function
    customer_age_main()
