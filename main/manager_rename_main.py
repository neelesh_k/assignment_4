# -*- coding: utf-8 -*-
"""
Rename the name of a manager
"""
import json
import logging
from helper import initialize_logger, initialize_command_args_staff_rename
from solution.queries.manager_rename import rename_manager
from solution.object_creation.transaction_objects import create_objects

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def staff_rename_main():
    # construct objects and serialize as transactions.json file
    create_objects()

    old_first_name, old_last_name, new_first_name, new_last_name = initialize_command_args_staff_rename()

    # loading transactions.json file into a list
    with open("transactions.json") as json_file:
        transaction_json_list = json.load(json_file)

    # Cross-check the transactions made by the staff and calculate the discrepancy if any
    LOGGER.info(rename_manager(transaction_json_list, old_first_name, old_last_name, new_first_name, new_last_name))


if __name__ == '__main__':
    # initialize logger
    initialize_logger()

    # main function
    staff_rename_main()
