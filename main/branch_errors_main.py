# -*- coding: utf-8 -*-
"""
Transaction discrepancy in branches
---------------------------------------
"""
import json
import logging
from helper import initialize_logger, initialize_command_args_branch_error
from solution.queries.branch_errors import get_branch_error
from solution.object_creation.transaction_objects import create_objects

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def branch_errors_main():
    # construct objects and serialize as transactions.json file
    # create_objects()

    min_discount = initialize_command_args_branch_error()

    # loading transactions.json file into a list
    with open("transactions.json") as json_file:
        transaction_json_list = json.load(json_file)

    # Cross-check the transactions made by the staff and calculate the discrepancy if any
    LOGGER.info(get_branch_error(transaction_json_list, min_discount))


if __name__ == '__main__':
    # initialize logger
    initialize_logger()

    # main function
    branch_errors_main()
