# -*- coding: utf-8 -*-
"""
Total revenue between two dates
--------------------------------

Filter the transactions between two dates and get the total amount in INR
"""
# Importing libraries
import json
import logging
from helper import initialize_logger, initialize_command_args_transactions
from solution.queries.transaction_inr import get_transaction_inr
from solution.object_creation.transaction_objects import create_objects

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def transactions_main():
    # construct objects and serialize as transactions.json file
    # create_objects()

    # loading transactions.json file into a list
    with open("transactions.json") as json_file:
        transaction_json_list = json.load(json_file)

    start_date, end_date, exchange_rate = initialize_command_args_transactions()

    # Find the transaction amounts (in USD and INR) between the given two dates.
    LOGGER.info(get_transaction_inr(transaction_json_list, start_date, end_date, exchange_rate))


if __name__ == '__main__':
    # initialize logger
    initialize_logger()

    # main function
    transactions_main()
