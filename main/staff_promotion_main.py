# -*- coding: utf-8 -*-
"""
Find the staff that generated the most revenue from a particular product and promote him/her
"""
import json
import logging
from helper import initialize_logger
from solution.queries.staff_promotion import promote_best_employee
from solution.object_creation.transaction_objects import create_objects

__author__ = "neelesh@gyandata.com"

# Get logger
LOGGER = logging.getLogger(name=__name__)


def staff_performance_main():
    # construct objects and serialize as transactions.json file
    create_objects()
    # loading transactions.json file into a list
    with open("transactions.json") as json_file:
        transaction_json_list = json.load(json_file)

    # Cross-check the transactions made by the staff and calculate the discrepancy if any
    LOGGER.info(promote_best_employee(transaction_json_list))


if __name__ == '__main__':
    # initialize logger
    initialize_logger()

    # main function
    staff_performance_main()
